﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Gui
{
    public  class SpeedControlButton : MonoBehaviour
    {
        public float TimeScale = 1f;

        private Toggle _toggle;

        void Start()
        {
            _toggle = GetComponent<Toggle>();
            _toggle.onValueChanged.AddListener((val) =>
            {
                var colorBlock = _toggle.colors;
                if (val)
                {
                    Time.timeScale = TimeScale;
                    colorBlock.normalColor = Color.red;
                    colorBlock.highlightedColor = Color.red;
                }
                else
                {
                    colorBlock.normalColor = Color.white;
                    colorBlock.highlightedColor = Color.white;
                }

                _toggle.colors = colorBlock;
            });

            if (_toggle.isOn)
            {
                var colorBlock = _toggle.colors;
                colorBlock.normalColor = Color.red;
                colorBlock.highlightedColor = Color.red;
                _toggle.colors = colorBlock;
                Time.timeScale = TimeScale;
            }
        }
    }
}
