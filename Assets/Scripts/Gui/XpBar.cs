﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Gui
{
    public class XpBar : MonoBehaviour
    {
        private Image _image;

        void Awake()
        {
            _image = GetComponent<Image>();
        }

        public void SetXp(float currentXp, float maxXp)
        {
            var scale = currentXp / maxXp;
            _image.transform.localScale = new Vector3(scale, 1);
        }
    }
}
