﻿using Assets.Scripts.Common;
using Assets.Scripts.Gui;
using Assets.Scripts.PlaneMechanics;
using Assets.Scripts.SaveAndLoad;
using Assets.Scripts.Stages;
using Assets.Scripts.Turrets;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using EventType = Assets.Scripts.Common.EventType;

namespace Assets.Scripts
{
    public class Game : MonoBehaviour
    {
        private static Game _instance;
        public static Game Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<Game>();
                }

                return _instance;
            }
        }

        public Text ScoreText;
        public Text GoldText;
        public Canvas FloatingScoreCanvas;
        public Text FloatingScorePrefab;
        public XpBar XpBar;
        public Text LevelText;

        public GameObject LoseScreen;
        public GameObject WinScreen;

        public int CurrentScore { get; private set; }
        public int CurrentGold { get; private set; }
        public float ViewPortWidth { get; private set; }

        public PlaneTracker PlaneTracker { get; private set; }
        public TurretTracker TurretTracker { get; private set; }

        private readonly GameSaver _gameSaver = new GameSaver();
        private Player _player;

        void Awake()
        {
            PlaneTracker = FindObjectOfType<PlaneTracker>();
            TurretTracker = FindObjectOfType<TurretTracker>();
        }

        void Start()
        {

            _player = PlayerManager.Instance.Player;
            var nextLevel = Levels.GetLevel(_player.CurrentLevel + 1);

            ViewPortWidth = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, 0)).x * 2;
            XpBar.SetXp(_player.CurrentXp, nextLevel.XpNeeded);
            LevelText.text = "Level " + _player.CurrentLevel;

            AddGold(1500);
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
        }

        public bool Defeat { get; private set; }

        public void AddScore(int score, Vector2? floatAtPosition)
        {
            CurrentScore += score;
            ScoreText.text = CurrentScore.ToString("0");

            if (floatAtPosition != null)
            {
                var floatingScore = Instantiate(FloatingScorePrefab);
                floatingScore.text = score.ToString("0");
                floatingScore.transform.position = new Vector2(floatAtPosition.Value.x, floatAtPosition.Value.y + 4);
                floatingScore.transform.SetParent(FloatingScoreCanvas.transform, true);
                Destroy(floatingScore.gameObject, 2);
            }
        }

        public void AddGold(int gold)
        {
            CurrentGold += gold;
            GoldText.text = CurrentGold.ToString("0");
            Events.Instance.TriggerEvent(EventType.GoldAdded);
        }

        public void Restart()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        public void Lose()
        {
            Defeat = true;
            _gameSaver.Save(_player);
            LoseScreen.gameObject.SetActive(true);
        }

        public void Win()
        {
            if (Defeat)
            {
                return;
            }

            if (BattleManager.Instance.CurrentBattle != null)
            {
                
                if (!_player.CompletedBattleIdentifiers.Contains(BattleManager.Instance.CurrentBattle.Identifier))
                {
                    _player.CompletedBattleIdentifiers.Add(BattleManager.Instance.CurrentBattle.Identifier);
                }
            }
            else
            {
                //This will happen if, for testing, we launch the battle directly from the scene without using the map
                //Log it just in case because this should never happen in real life
                Debug.Log("Battle manager current battle was null - No battle will be saved for the player");
            }
            
            _gameSaver.Save(_player);
            WinScreen.gameObject.SetActive(true);
        }

        public void AddXp(int xp)
        {
            _player.CurrentXp += xp;
            if (Levels.ShouldLevelUp(_player.CurrentLevel, _player.CurrentXp))
            {
                var nextLevel = Levels.GetLevel(_player.CurrentLevel + 1);
                _player.CurrentXp = _player.CurrentXp - nextLevel.XpNeeded;
                _player.CurrentLevel++;

                LevelText.text = "Level " + _player.CurrentLevel;

                nextLevel = Levels.GetLevel(_player.CurrentLevel + 1);
                if (nextLevel != null)
                {
                    XpBar.SetXp(_player.CurrentXp, nextLevel.XpNeeded);
                }
                else
                {
                    XpBar.SetXp(_player.CurrentXp, _player.CurrentXp);
                }
                Events.Instance.TriggerEvent(EventType.LevelUp);
                _gameSaver.Save(_player);
            }
            else
            {
                XpBar.SetXp(_player.CurrentXp, Levels.GetLevel(_player.CurrentLevel + 1).XpNeeded);
            }
            Events.Instance.TriggerEvent(EventType.XpAdded);
        }
    }
}
