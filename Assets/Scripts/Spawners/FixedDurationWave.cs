﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.PlaneMechanics;

namespace Assets.Scripts.Spawners
{
    [Serializable]
    public class FixedDurationWave
    {
        public List<SpawnRateWaveGroup> WaveGroups = new List<SpawnRateWaveGroup>();

        public float WaveDuration;
    }

    [Serializable]
    public class SpawnRateWaveGroup
    {
        public Plane PlanePrefab;
        public float SpawnRate;

        public float TimeSinceLastSpawn { get; set; }
    }
}
