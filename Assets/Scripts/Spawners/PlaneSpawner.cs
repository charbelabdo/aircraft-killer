﻿using Assets.Scripts.PlaneMechanics;
using UnityEngine;
using Plane = Assets.Scripts.PlaneMechanics.Plane;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Spawners
{
    public class PlaneSpawner : MonoBehaviour
    {
        public float Rate = 2f;
        public Plane PlanePrefab;
        
        private float _timeSinceLastSpawn = 1000f;
        private PlaneTracker _planeTracker;

        void Awake()
        {
            _planeTracker = FindObjectOfType<PlaneTracker>();
        }

        void Update()
        {
            Rate = Mathf.Max(Rate - (Time.deltaTime / 100), 0.5f);
            _timeSinceLastSpawn += Time.deltaTime;
            if (_timeSinceLastSpawn >= Rate)
            {
                var plane = Instantiate(PlanePrefab);
                var randomY = Random.Range(-10, 10);
                plane.transform.position = new Vector3(transform.position.x, transform.position.y + randomY);
                plane.transform.SetParent(Game.Instance.PlaneTracker.transform, true);

                plane.Fly();
                _timeSinceLastSpawn = 0;
            }
        }
    }
}
