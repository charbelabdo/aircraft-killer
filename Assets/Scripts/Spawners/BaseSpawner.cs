﻿
using UnityEngine;
using Plane = Assets.Scripts.PlaneMechanics.Plane;

namespace Assets.Scripts.Spawners
{
    public abstract class BaseSpawner : MonoBehaviour, ISpawner
    {
        public abstract void SpawnIfNeeded();
        public abstract bool IsDone();

        protected void Spawn(Plane planePrefab)
        {
            var plane = Instantiate(planePrefab);
            var randomY = Random.Range(-10, 10);
            plane.transform.position = new Vector3(transform.position.x, transform.position.y + randomY);
            plane.transform.SetParent(Game.Instance.PlaneTracker.transform, true);

            plane.Fly();
        }
    }
}
