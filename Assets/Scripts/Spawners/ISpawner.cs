﻿namespace Assets.Scripts.Spawners
{
    public interface ISpawner
    {
        void SpawnIfNeeded();
        bool IsDone();
    }
}
