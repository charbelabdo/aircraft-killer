﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Spawners
{
    public class TimedWaveSpawner : BaseSpawner, ISpawner
    {
        public List<FixedDurationWave> Waves = new List<FixedDurationWave>();

        private float _currentWaveTime = 0f;

        public override void SpawnIfNeeded()
        {
            if (!Waves.Any())
            {
                return;
            }

            _currentWaveTime += Time.deltaTime;
            foreach (var waveGroup in Waves[0].WaveGroups)
            {
                waveGroup.TimeSinceLastSpawn += Time.deltaTime;

                if (waveGroup.TimeSinceLastSpawn >= waveGroup.SpawnRate)
                {
                    Spawn(waveGroup.PlanePrefab);
                    waveGroup.TimeSinceLastSpawn = 0f;
                }
            }

            if (_currentWaveTime > Waves[0].WaveDuration)
            {
                Waves.RemoveAt(0);
                _currentWaveTime = 0f;
            }

        }

        public override bool IsDone()
        {
            return !Waves.Any();
        }
    }
}
