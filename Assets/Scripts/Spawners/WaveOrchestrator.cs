﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Spawners
{
    public class WaveOrchestrator : MonoBehaviour
    {
        private List<ISpawner> _spawners = new List<ISpawner>();

        void Awake()
        {
            _spawners = GetComponentsInChildren<ISpawner>().ToList();
        }

        void Update()
        {
            if (!_spawners.Any())
            {
                if (!Game.Instance.PlaneTracker.AnyLivingPlane && !Game.Instance.Defeat)
                {
                    Game.Instance.Win();
                    Destroy(gameObject);
                }
            }
            else
            {
                if (_spawners[0].IsDone())
                {
                    _spawners.RemoveAt(0);
                }
                else
                {
                    _spawners[0].SpawnIfNeeded();
                }
            }

        }
    }
}
