﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public class NewGameCreator
    {
        public Player CreateNewPlayer()
        {
            return new Player
            {
                CurrentXp = 0,
                Gold = 0,
                CompletedBattleIdentifiers = new List<string>(),
                CurrentLevel = 1,
                OwnedTurretIdentifiers = new List<string>
                {
                    "AA1",
                    "Missile1",
                    "AA2",
                    "Missile2",
                    "Interceptor1"
                }
            };
        }
    }
}
