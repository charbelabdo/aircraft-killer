﻿using Assets.Scripts.LifeAndDeath;
using UnityEngine;

namespace Assets.Scripts.Projectiles
{
    public class Bullet : MonoBehaviour
    {
        public float Damage = 20;

        void Start()
        {
            Destroy(gameObject, 5);
        }

        void OnCollisionEnter2D(Collision2D collision)
        {
            var health = collision.gameObject.GetComponent<Health>();
            if (health != null)
            {
                health.DoDamage(Damage, collision.GetContact(0).point);
            }
            Destroy(gameObject);
        }
    }
}
