﻿using System.Linq;
using Assets.Scripts.LifeAndDeath;
using UnityEngine;

namespace Assets.Scripts.Projectiles
{
    public class Explosion : MonoBehaviour
    {
        public AudioClip ExplosionSound;

        public void Explode(float damageAtCenter, float radius)
        {
            AudioSource.PlayClipAtPoint(ExplosionSound, Vector3.zero, 1);

            //To differentiate with effect only explosions
            if (damageAtCenter > 0 && radius > 0)
            {
                var inRange = Physics2D.OverlapCircleAll(transform.position, radius).ToList();
                foreach (var obj in inRange)
                {
                    var health = obj.GetComponent<Health>();
                    if (health != null)
                    {
                        health.DoDamage(damageAtCenter, obj.transform.position);
                    }
                }
            }

            Destroy(gameObject, 5);
        }
    }
}
