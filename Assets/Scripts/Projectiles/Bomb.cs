﻿using UnityEngine;

namespace Assets.Scripts.Projectiles
{
    public class Bomb : MonoBehaviour
    {
        public Explosion ExplosionPrefab;

        public float Damage;
        public float ExplosionRange;

        void OnCollisionEnter2D(Collision2D collision)
        {
            var explosion = Instantiate(ExplosionPrefab);
            explosion.transform.position = transform.position;
            explosion.Explode(Damage, ExplosionRange);
            Destroy(gameObject);
        }
    }
}
