﻿using Assets.Scripts.Common;
using UnityEngine;

namespace Assets.Scripts.Projectiles
{
    public class Missile : MonoBehaviour
    {
        public float TargetDetectionRange;
        public Explosion ExplosionPrefab;
        private GameObject _target;
        public float ExplosionDamage;
        public float ExplosionRange;

        private MissileMove _missileMove;
        private bool _launched = false;

        void Awake()
        {
            _missileMove = GetComponent<MissileMove>();
        }

        void Update()
        {
            if (!_launched)
            {
                return;
            }

            if (_target == null)
            {
                FindTarget();
            }
        }

        public void Launch(Vector2 direction)
        {
            _launched = true;
            _missileMove.Launch(direction);
            Invoke("Explode", 10);
        }

        private void FindTarget()
        {
            var objects = Physics2D.OverlapCircleAll(transform.position, TargetDetectionRange);

            foreach (var obj in objects)
            {
                var missileTarget = obj.GetComponent<MissileTarget>();
                if (missileTarget != null && missileTarget.tag != gameObject.tag)
                {
                    _target = missileTarget.gameObject;
                    break;
                }
            }

            if (_target != null)
            {
                SetTarget(_target.gameObject);
            }
        }

        void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.tag != gameObject.tag)
            {
                Explode();
            }
        }

        private void Explode()
        {
            var explosion = Instantiate(ExplosionPrefab);
            explosion.transform.position = transform.position;
            explosion.Explode(ExplosionDamage, ExplosionRange);
            Destroy(gameObject);
        }

        public void SetTarget(GameObject target)
        {
            _target = target;
            if (target != null)
            {
                _missileMove.SetTarget(target);
            }
        }
    }
}
