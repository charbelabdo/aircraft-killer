﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Projectiles
{
    public class MissileMove : MonoBehaviour
    {
        public bool HighAccuracy = false;

        private GameObject _target;
        private Rigidbody2D _rigidbody;

        public float Speed;

        void Awake()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
        }

        void Update()
        {
            if (_target != null)
            {
                var targetDirection = _target.transform.position - transform.position;
                var currentDirection = _rigidbody.velocity.normalized;
                var newDirection = HighAccuracy? (Vector2)targetDirection.normalized : Vector2.Lerp(currentDirection, targetDirection, Time.deltaTime);
                _rigidbody.velocity = newDirection * Speed;
            }
        }

        public void Launch(Vector2 direction)
        {
            _rigidbody.velocity = direction.normalized * Speed;
        }

        public void SetTarget(GameObject target)
        {
            _target = target;
        }
    }
}
