﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.Stages
{
    public class BattleButton : MonoBehaviour
    {
        private Button _button;

        public BattleData BattleData;

        void Start()
        {
            _button = GetComponent<Button>();
            _button.onClick.AddListener(() =>
            {
                BattleManager.Instance.LoadBattle(BattleData);
            });
        }
    }
}
