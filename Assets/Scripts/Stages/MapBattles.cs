﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Stages
{
    public class MapBattles : MonoBehaviour
    {
        private static MapBattles _instance;

        public static MapBattles Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<MapBattles>();
                }

                return _instance;
            }
        }


        void Start()
        {
            var buttons = GetComponentsInChildren<BattleButton>().ToList();
            var completedBattles = PlayerManager.Instance.Player.CompletedBattleIdentifiers;

            foreach (var battleButton in buttons)
            {
                var prerequesiteDone = battleButton.BattleData.Prerequesites == null || battleButton.BattleData.Prerequesites.All(x => completedBattles.Contains(x.Identifier));
                var done = completedBattles.Contains(battleButton.BattleData.Identifier);

                if (done)
                {
                    var button = battleButton.GetComponent<Button>();
                    var color = button.colors;
                    color.normalColor = Color.green;
                    button.colors = color;
                }

                if (!prerequesiteDone)
                {
                    battleButton.gameObject.SetActive(false);
                }
            }
        }
    }
}
