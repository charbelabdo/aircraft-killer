﻿using UnityEngine;

namespace Assets.Scripts.Stages
{
    [CreateAssetMenu]
    public class BattleData : ScriptableObject
    {
        public string Identifier;

        public string Name;

        public string Location;

        [TextArea(5, 10)]
        public string Description;

        public BattleDifficulty Difficulty;

        public BattleData[] Prerequesites;

        public string ObjectiveDescription;

        public ObjectiveType ObjectiveType;

        public BattleData ChallengeModelBattle;

        public BattleData HordeModeBattle;
    }
}
