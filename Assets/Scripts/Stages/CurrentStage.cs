﻿using UnityEngine.SceneManagement;

namespace Assets.Scripts.Stages
{
    public class BattleManager
    {
        private static BattleManager _instance;
        public BattleData CurrentBattle { get; private set; }


        public static BattleManager Instance
        {
            get { return _instance ?? (_instance = new BattleManager()); }
        }

        private BattleManager()
        {

        }

        public void LoadBattle(BattleData battleData)
        {
            CurrentBattle = battleData;
            SceneManager.LoadScene(battleData.Identifier);
        }
    }
}
