﻿namespace Assets.Scripts.Stages
{
    public enum ObjectiveType
    {
        DontLetPlanesPass,
        DefendBuilding,
        ShootDownXPlanes
    }
}
