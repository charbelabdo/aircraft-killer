﻿namespace Assets.Scripts.Stages
{
    public enum BattleDifficulty
    {
        Easy,
        Intermediate,
        Hard
    }
}
