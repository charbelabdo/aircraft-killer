﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Common;
using Assets.Scripts.Turrets;
using UnityEngine;

namespace Assets.Scripts.PlaneMechanics
{
    [RequireComponent(typeof(Plane))]
    public class DiveBomber : MonoBehaviour
    {
        private const float BeginDivePosition = 50;
        private const float DropAltitude = 10;
        private const float ClimbAngleDegrees = 40;

        private DiveStage _diveStage = DiveStage.None;
        private Plane _plane;

        private float _diveTurnTime;
        private float _initialAltitude;

        private TurretBase _target;
        private List<BombBay> _bombingBays;
        private readonly RandomTurretFinder _turretFinder = new RandomTurretFinder();
        private readonly PlaneDive _planeDive = new PlaneDive();
        private readonly PlaneClimb _planeClimb = new PlaneClimb();

        public bool KamikazeMode = false;

        void Start()
        {
            _plane = GetComponent<Plane>();
            _bombingBays = GetComponentsInChildren<BombBay>().ToList();
        }

        void Update()
        {
            _target = _target ?? _turretFinder.GetTurret();
            if (_target == null)
            {
                if (_diveStage == DiveStage.Diving)
                {
                    _diveStage = DiveStage.Climbing;
                }
            }

            if (_plane.Dying)
            {
                Destroy(this);
                return;
            }

            switch (_diveStage)
            {
                case DiveStage.None:
                    {
                        if (_target != null && _target.transform.position.x - transform.position.x < BeginDivePosition)
                        {
                            _initialAltitude = _plane.transform.position.y;
                            _diveStage = DiveStage.Diving;
                        }
                        break;
                    }
                case DiveStage.Diving:
                    {
                        if (_target != null)
                        {
                            DiveTowards();
                        }
                        else
                        {
                            _diveStage = DiveStage.Climbing;
                            _planeDive.Reset();
                        }

                        break;
                    }
                case DiveStage.Climbing:
                    {
                        //Just let the plane dive to its death
                        if (KamikazeMode)
                        {
                            return;
                        }

                        _planeClimb.Climb(ClimbAngleDegrees, _plane);

                        if (Math.Abs(_plane.transform.position.y - _initialAltitude) < 1)
                        {
                            _planeClimb.Reset();
                            _diveStage = DiveStage.Cruise;
                        }

                        break;
                    }
                case DiveStage.Cruise:
                    {
                        _diveTurnTime += Time.deltaTime / 3;
                        var currentDirection = _plane.Velocity.normalized;
                        var targetDirection = new Vector2(1, 0).normalized;

                        var nextDirection = Vector2.Lerp(currentDirection, targetDirection, _diveTurnTime);
                        _plane.SetDirection(nextDirection, 1);
                        break;
                    }
            }
        }

        private void DiveTowards()
        {
            _planeDive.DiveTowards(_target.transform.position, transform.position, _plane, 2);

            if (transform.position.y - _target.transform.position.y < DropAltitude)
            {
                _diveStage = DiveStage.Climbing;
                foreach (var bombingBay in _bombingBays)
                {
                    bombingBay.ReleaseBomb();
                }

                _diveTurnTime = 0f;
            }
        }
    }

    public enum DiveStage
    {
        None = 0,
        Diving = 1,
        Climbing = 2,
        Cruise = 3
    }
}
