﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Common;
using Assets.Scripts.Turrets;
using UnityEngine;

namespace Assets.Scripts.PlaneMechanics
{
    [RequireComponent(typeof(Plane))]
    public class CarpetBomber : MonoBehaviour
    {
        private RandomTurretFinder _randomTurretFinder = new RandomTurretFinder();
        private List<BombBay> _bombingBays;
        private float _timeSinceLastBomb = 0;
        private Plane _plane;
        private TurretBase _target;
        private float _beginBombingDistance;

        public float Rate = 0.75f;
        public int NumberOfBombs = 10;
        


        void Start()
        {
            _bombingBays = GetComponentsInChildren<BombBay>().ToList();
            _plane = GetComponent<Plane>();
        }

        void Update()
        {
            if (_plane.Dying)
            {
                Destroy(this);
                return;
            }

            if (NumberOfBombs > 0)
            {
                _target = _target ?? GetTarget();

                if (_target != null)
                {
                    if (_target.transform.position.x - transform.position.x < _beginBombingDistance)
                    {
                        _timeSinceLastBomb += Time.deltaTime;
                        if (_timeSinceLastBomb >= Rate)
                        {
                            _timeSinceLastBomb = 0f;
                            foreach (var bombingBay in _bombingBays)
                            {
                                bombingBay.ReleaseBomb();
                            }

                            NumberOfBombs--;
                        }
                    }
                }
            }
        }

        private TurretBase GetTarget()
        {
            var target = _randomTurretFinder.GetTurret();
            if (target != null)
            {
                _beginBombingDistance = _plane.Velocity.magnitude * 4;
            }

            return target;
        }
    }
}
