﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.PlaneMechanics
{
    public class PlaneDive
    {
        private float _diveTurnTime;

        public void DiveTowards(Vector2 targetPosition, Vector2 currentPosition, Plane plane, float speedFactor)
        {
            _diveTurnTime += Time.deltaTime / 3;
            var directionVector = targetPosition - currentPosition;
            var targetDirection = Vector2.Lerp(plane.Velocity, directionVector, _diveTurnTime);
            plane.SetDirection(targetDirection, speedFactor);
        }

        public void Reset()
        {
            _diveTurnTime = 0f;
        }
    }
}
