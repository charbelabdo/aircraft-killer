﻿using Assets.Scripts.Common;
using Assets.Scripts.Projectiles;
using Assets.Scripts.Turrets;
using UnityEngine;

namespace Assets.Scripts.PlaneMechanics
{
    public class MissileJet : MonoBehaviour
    {
        public Missile MissilePrefab;
        public float HorizontalDistanceToTarget = 200f;

        private RandomTurretFinder _randomTurretFinder = new RandomTurretFinder();
        private TurretBase _target;
        private Plane _plane;

        private bool _fired = false;

        void Awake()
        {
            _plane = GetComponent<Plane>();
        }

        void Update()
        {
            if (_fired)
            {
                return;
            }

            _target = _target ?? _randomTurretFinder.GetTurret();
            if (_target == null)
            {
                return;
            }

            if (_target.transform.position.x - transform.position.x < HorizontalDistanceToTarget)
            {
                var missile = Instantiate(MissilePrefab);
                missile.gameObject.layer = LayerMask.NameToLayer(Layers.EnemyProjectiles);
                missile.SetTarget(_target.gameObject);
                missile.transform.position = new Vector2(transform.position.x, transform.position.y - 1);
                missile.gameObject.tag = Tags.EnemyTag;
                Vector2 direction = _target.transform.position - transform.position;
                var midway = (_plane.Velocity + direction) / 2;
                missile.Launch(midway);
                _fired = true;
            }
        }
    }
}
