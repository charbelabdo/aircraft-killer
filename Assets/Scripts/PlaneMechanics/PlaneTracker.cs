﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Common;
using UnityEngine;

namespace Assets.Scripts.PlaneMechanics
{
    public class PlaneTracker : MonoBehaviour
    {
        private readonly List<Plane> _planes = new List<Plane>();
        
        public bool AnyLivingPlane
        {
            get { return _planes.Any(x => x != null && !x.Dying); }
        }

        private void RegisterPlane(Plane plane)
        {
            plane.gameObject.AddComponent<MissileTarget>();
            _planes.Add(plane);
        }

        public void UnregisterPlane(Plane plane)
        {
            _planes.Remove(plane);
        }

        public Plane GetRandomPlane()
        {
            if (!_planes.Any())
            {
                return null;
            }

            var rnd = Random.Range(0, _planes.Count);
            return _planes[rnd];
        }

        void OnTriggerEnter2D(Collider2D col)
        {
            var plane = col.gameObject.GetComponent<Plane>();
            if (plane != null)
            {
                RegisterPlane(plane);
            }
        }
    }
}
