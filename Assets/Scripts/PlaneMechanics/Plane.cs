﻿using System;
using Assets.Scripts.Common;
using Assets.Scripts.LifeAndDeath;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.PlaneMechanics
{
    public class Plane : MonoBehaviour
    {
        public float MinSpeed = 5;
        public float MaxSpeed = 25;
        public TrailRenderer DeathTrail;

        public Vector2 Velocity
        {
            get { return _rigidBody.velocity; }
        }

        private Health _health;

        public bool Dying { get; private set; }
        private Rigidbody2D _rigidBody;
        private float _speed;

        void Awake()
        {
            _health = GetComponent<Health>();
            _rigidBody = GetComponent<Rigidbody2D>();
            this.gameObject.tag = Tags.EnemyTag;
        }

        public void Fly()
        {
            _speed = Random.Range(MinSpeed, MaxSpeed);
            _rigidBody.velocity = new Vector2(_speed, 0);
        }

        public void SetDirection(Vector2 direction, float speedMultiplier)
        {
            if (!Dying)
            {
                _rigidBody.velocity = direction.normalized * _speed * speedMultiplier;
            }
        }

        void Update()
        {
            if (!Dying)
            {
                if (_health.CurrentHealth < _health.MaxHealth / 3)
                {
                    Dying = true;
                    _rigidBody.bodyType = RigidbodyType2D.Dynamic;

                    var bombingBays = GetComponentsInChildren<BombBay>();
                    foreach (var bombingBay in bombingBays)
                    {
                        Destroy(bombingBay);
                    }

                    if (DeathTrail != null)
                    {
                        DeathTrail.gameObject.SetActive(true);
                    }
                }
            }
        }

        void OnCollisionEnter2D(Collision2D collision)
        {
            var ground = collision.gameObject.GetComponent<Ground>();
            if (ground != null)
            {
                _health.DoDamage(_health.CurrentHealth, collision.GetContact(0).point);
            }
        }

        void OnDestroy()
        {
            if (Game.Instance == null || Game.Instance.PlaneTracker == null)
            {
                return;
            }

            Game.Instance.PlaneTracker.UnregisterPlane(this);
        }
    }
}
