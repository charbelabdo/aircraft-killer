﻿using UnityEngine;

namespace Assets.Scripts.PlaneMechanics
{
    public class PlaneClimb
    {
        private float _diveTurnTime;

        public void Climb(float angleDegrees, Plane plane)
        {
            _diveTurnTime += Time.deltaTime / 3;
            var currentDirection = plane.Velocity.normalized;
            var targetDirection = new Vector2(Mathf.Cos(angleDegrees * Mathf.Deg2Rad), Mathf.Sin(angleDegrees * Mathf.Deg2Rad)).normalized;

            var nextDirection = Vector2.Lerp(currentDirection, targetDirection, _diveTurnTime);
            plane.SetDirection(nextDirection, 0.75f);
        }

        public void Reset()
        {
            _diveTurnTime = 0f;
        }
    }
}