﻿using Assets.Scripts.Common;
using Assets.Scripts.Projectiles;
using UnityEngine;

namespace Assets.Scripts.PlaneMechanics
{
    public class BombBay : MonoBehaviour
    {
        public Bomb BombPrefab;

        private Rigidbody2D _rigidbody2D;

        void Start()
        {
            _rigidbody2D = GetComponentInParent<Rigidbody2D>();
        }

        public void ReleaseBomb()
        {
            var bomb = Instantiate(BombPrefab);
            bomb.gameObject.layer = LayerMask.NameToLayer(Layers.EnemyProjectiles);
            bomb.transform.position = new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z);

            var bombRigidbody = bomb.gameObject.GetComponent<Rigidbody2D>();
            bombRigidbody.velocity = _rigidbody2D.velocity;
        }
    }
}
