﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Common;
using Assets.Scripts.Projectiles;
using Assets.Scripts.Turrets;
using UnityEngine;

namespace Assets.Scripts.PlaneMechanics
{
    [RequireComponent(typeof(Plane))]
    public class Strafer : MonoBehaviour
    {
        private const float BeginDivePosition = 60;
        private const float ClimbAngleDegrees = 40;
        private const float BeginFireDistance = 50;
        private const float EndFireDistance = 10;

        public Bullet BulletPrefab;
        public float RateOfFire = 0.2f;

        private readonly RandomTurretFinder _turretFinder = new RandomTurretFinder();
        private TurretBase _target;
        private readonly PlaneDive _planeDive = new PlaneDive();
        private readonly PlaneClimb _planeClimb = new PlaneClimb();
        private Plane _plane;
        private List<Muzzle> _muzzles;
        private StrafeStage _strafeStage = StrafeStage.None;
        private float _timeSinceLastFire = 1000f;
        private float _initialAltitude;
        private float _diveTurnTime;

        void Awake()
        {
            _plane = GetComponent<Plane>();
            _muzzles = GetComponentsInChildren<Muzzle>().ToList();
        }

        void Update()
        {
            _timeSinceLastFire += Time.deltaTime;
            _target = _target ?? _turretFinder.GetTurret();
            if (_target == null)
            {
                if (_strafeStage == StrafeStage.Diving || _strafeStage == StrafeStage.Strafing)
                {
                    _strafeStage = StrafeStage.Climbing;
                }
            }

            if (_plane.Dying)
            {
                Destroy(this);
                return;
            }

            switch (_strafeStage)
            {
                case StrafeStage.None:
                    {
                        if (_target != null && _target.transform.position.x - transform.position.x < BeginDivePosition)
                        {
                            _initialAltitude = _plane.transform.position.y;
                            _strafeStage = StrafeStage.Diving;
                        }

                        break;
                    }
                case StrafeStage.Diving:
                    {
                        if (_target != null)
                        {
                            _planeDive.DiveTowards(_target.transform.position, transform.position, _plane, 1.2f);
                            if (Vector2.Distance(_target.transform.position, transform.position) < BeginFireDistance)
                            {
                                var targetDirection = _target.transform.position - transform.position;
                                if (Vector2.Angle(_plane.Velocity, targetDirection) < 2)
                                {
                                    _strafeStage = StrafeStage.Strafing;
                                    _planeDive.Reset();
                                }
                            }
                        }
                        else
                        {
                            _strafeStage = StrafeStage.Climbing;
                            _planeDive.Reset();
                        }
                        break;
                    }
                case StrafeStage.Strafing:
                    {
                        if (_target == null)
                        {
                            _strafeStage = StrafeStage.Climbing;
                            return;
                        }

                        if (_timeSinceLastFire >= RateOfFire)
                        {
                            foreach (var muzzle in _muzzles)
                            {
                                var newBullet = Instantiate(BulletPrefab);
                                newBullet.gameObject.layer = LayerMask.NameToLayer(Layers.EnemyBullets);
                                newBullet.transform.position = muzzle.transform.position;
                                var bulletRigidBody = newBullet.GetComponent<Rigidbody2D>();
                                bulletRigidBody.velocity = _plane.Velocity.normalized * 100;
                                _timeSinceLastFire = 0f;
                            }
                        }

                        var distance = Vector2.Distance(_target.transform.position, transform.position);
                        if (distance < EndFireDistance)
                        {
                            _strafeStage = StrafeStage.Climbing;
                        }

                        break;
                    }
                case StrafeStage.Climbing:
                    {
                        _planeClimb.Climb(ClimbAngleDegrees, _plane);

                        if (Math.Abs(_plane.transform.position.y - _initialAltitude) < 1)
                        {
                            _planeClimb.Reset();
                            _strafeStage = StrafeStage.Cruise;
                            _diveTurnTime = 0f;
                        }
                        break;
                    }
                case StrafeStage.Cruise:
                    {
                        _diveTurnTime += Time.deltaTime / 3;
                        var currentDirection = _plane.Velocity.normalized;
                        var targetDirection = new Vector2(1, 0).normalized;

                        var nextDirection = Vector2.Lerp(currentDirection, targetDirection, _diveTurnTime);
                        _plane.SetDirection(nextDirection, 1);
                        break;
                    }
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

    }

    public enum StrafeStage
    {
        None = 0,
        Diving = 1,
        Strafing = 2,
        Climbing = 3,
        Cruise = 4
    }
}
