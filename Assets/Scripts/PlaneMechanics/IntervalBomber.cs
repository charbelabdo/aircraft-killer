﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.PlaneMechanics
{
    [RequireComponent(typeof(Plane))]
    public class IntervalBomber : MonoBehaviour
    {
        public float Rate = 3f;
        private List<BombBay> _bombingBays;
        private float _timeSinceLastBomb = 0;
        private Plane _plane;

        void Start()
        {
            _bombingBays = GetComponentsInChildren<BombBay>().ToList();
            _plane = GetComponent<Plane>();
        }

        void Update()
        {
            if (_plane.Dying)
            {
                Destroy(this);
                return;
            }

            _timeSinceLastBomb += Time.deltaTime;
            if (_timeSinceLastBomb >= Rate)
            {
                _timeSinceLastBomb = 0f;
                foreach (var bombingBay in _bombingBays)
                {
                    bombingBay.ReleaseBomb();
                }
            }
        }
    }
}
