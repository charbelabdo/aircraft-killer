﻿using System.IO;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.SaveAndLoad
{
    public class GameLoader
    {
        public bool SaveGameExists()
        {
            var filename = Application.persistentDataPath + "/save1.sav";
            return File.Exists(filename);
        }

        public Player Load()
        {
            var filename = Application.persistentDataPath + "/save1.sav";
            var bytes = File.ReadAllBytes(filename);
            var json = Encoding.UTF8.GetString(bytes);
            return JsonUtility.FromJson<Player>(json);
        }
    }
}