﻿using System.IO;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.SaveAndLoad
{
    public class GameSaver
    {
        public void Save(Player player)
        {
            string json = JsonUtility.ToJson(player);
            var bytes = Encoding.UTF8.GetBytes(json);
            var filename = Application.persistentDataPath + "/save1.sav";
            File.WriteAllBytes(filename, bytes);
        }
    }
}
