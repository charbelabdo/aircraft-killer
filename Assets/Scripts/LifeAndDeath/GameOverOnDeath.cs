﻿using UnityEngine;

namespace Assets.Scripts.LifeAndDeath
{
    public class GameOverOnDeath : MonoBehaviour, IDeathAction
    {
        public void OnDeath(Vector2 eventPosition)
        {
            Game.Instance.Lose();
        }
    }
}
