﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.LifeAndDeath
{
    public class HealthBar : MonoBehaviour
    {
        public bool HideWhenFull = true;

        private SpriteRenderer _spriteRenderer;
        private Image _image;
        private Vector3 _initialScale;

        void Awake()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
            if (_spriteRenderer != null)
            {
                _spriteRenderer.enabled = !HideWhenFull;
            }

            _initialScale = transform.localScale;
            _image = GetComponent<Image>();
        }

        public void SetHealth(float health, float max)
        {
            if (health < max && HideWhenFull && _spriteRenderer != null)
            {
                _spriteRenderer.enabled = true;
            }

            var rate = health / max;
            gameObject.transform.localScale  = new Vector3(_initialScale.x * rate, _initialScale.y, 1);

            if (rate > 0.8)
            {
               SetColor(Color.green);
            }
            else if (rate > 0.5)
            {
                SetColor(Color.yellow);
            }
            else if (rate > 0.3)
            {
                SetColor(new Color(1, 0.65f, 0));
            }
            else
            {
               SetColor(Color.red);
            }
        }

        private void SetColor(Color color)
        {
            if (_spriteRenderer != null)
            {
                _spriteRenderer.color = color;
            }

            if (_image != null)
            {
                _image.color = color;
            }
        }
    }
}
