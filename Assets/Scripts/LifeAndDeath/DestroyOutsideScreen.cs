﻿using UnityEngine;

namespace Assets.Scripts.LifeAndDeath
{
    public class DestroyOutsideScreen : MonoBehaviour
    {
        private float _maxX = 0f;

        void Awake()
        {
            var edgePoint = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0));
            _maxX = edgePoint.x;
        }

        void Update()
        {
            if (transform.position.x > _maxX + 20)
            {
                Destroy(gameObject);
            }
        }
    }
}
