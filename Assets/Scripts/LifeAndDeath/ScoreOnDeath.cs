﻿using UnityEngine;

namespace Assets.Scripts.LifeAndDeath
{
    public class ScoreOnDeath : MonoBehaviour, IDeathAction
    {
        public int KillScore;

        public void OnDeath(Vector2 eventPosition)
        {
            Game.Instance.AddScore(KillScore, transform.position);
        }
    }
}
