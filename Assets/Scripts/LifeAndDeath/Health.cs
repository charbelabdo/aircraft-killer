﻿using UnityEngine;

namespace Assets.Scripts.LifeAndDeath
{
    public class Health : MonoBehaviour
    {
        public float MaxHealth;
        public float CurrentHealth { get; private set; }
        public HealthBar HealthBar;
        private bool _isDead;

        void Start()
        {
            CurrentHealth = MaxHealth;
            HealthBar.SetHealth(MaxHealth, MaxHealth);
        }

        /// <summary>
        /// Does damage and returns the remaining health
        /// </summary>
        /// <param name="damage"></param>
        /// <param name="eventPosition">The position where the hit happened, typically used to show some effect</param>
        /// <returns></returns>
        public float DoDamage(float damage, Vector2 eventPosition)
        {
            if (_isDead)
            {
                return 0;
            }

            CurrentHealth -= damage;
            HealthBar.SetHealth(CurrentHealth, MaxHealth);
            if (CurrentHealth <= 0)
            {
                _isDead = true;
                CurrentHealth = 0;

                var deathComponents = GetComponents<IDeathAction>();
                foreach (var deathComponent in deathComponents)
                {
                    deathComponent.OnDeath(eventPosition);
                }

                Destroy(gameObject);
            }

            return CurrentHealth;
        }
    }
}
