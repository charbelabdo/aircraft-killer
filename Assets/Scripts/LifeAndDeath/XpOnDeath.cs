﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.LifeAndDeath
{
    public class XpOnDeath : MonoBehaviour, IDeathAction
    {
        public int Xp;

        public void OnDeath(Vector2 eventPosition)
        {
            Game.Instance.AddXp(Xp);
        }
    }
}
