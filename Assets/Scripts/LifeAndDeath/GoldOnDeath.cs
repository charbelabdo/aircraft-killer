﻿using UnityEngine;

namespace Assets.Scripts.LifeAndDeath
{
    public class GoldOnDeath : MonoBehaviour, IDeathAction
    {
        public int Gold;

        public void OnDeath(Vector2 eventPosition)
        {
            Game.Instance.AddGold(Gold);
        }
    }
}
