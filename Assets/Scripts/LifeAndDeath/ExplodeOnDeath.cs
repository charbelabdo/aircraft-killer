﻿using Assets.Scripts.Projectiles;
using UnityEngine;

namespace Assets.Scripts.LifeAndDeath
{
    public class ExplodeOnDeath : MonoBehaviour, IDeathAction
    {
        public Explosion ExplosionPrefab;

        public float ExplosionDamage = 0f;
        public float ExplosionRadius = 0f;

        public void OnDeath(Vector2 eventPosition)
        {
            var explosion = Instantiate(ExplosionPrefab);
            explosion.transform.position = eventPosition;
            explosion.Explode(ExplosionDamage, ExplosionRadius);
        }
    }
}
