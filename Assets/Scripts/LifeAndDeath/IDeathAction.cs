﻿using UnityEngine;

namespace Assets.Scripts.LifeAndDeath
{
    public interface IDeathAction
    {
        void OnDeath(Vector2 eventPosition);
    }
}
