﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Common
{
    public static class Tags
    {
        public const string TurretTag = "Turret";
        public const string EnemyTag = "Enemy";
    }
}
