﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Turrets;
using UnityEngine;

namespace Assets.Scripts.Common
{
    public class PopupManager : MonoBehaviour
    {
        private static PopupManager _instance;
        public static PopupManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<PopupManager>();
                }

                return _instance;
            }
        }


        public TurretBuildPopup TurretBuildPopup;

        public void OpenTurretBuildPopup(TurretSlot slot)
        {
            TurretBuildPopup.Open(slot);
        }
    }
}
