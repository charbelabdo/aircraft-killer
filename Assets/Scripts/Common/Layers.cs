﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Common
{
    public static class Layers
    {
        public const string EnemyProjectiles = "EnemyProjectile";
        public const string AllyProjectiles = "AllyProjectile";
        public const string AllyBullets = "AllyBullets";
        public const string EnemyBullets = "EnemyBullets";
    }
}
