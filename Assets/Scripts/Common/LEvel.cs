﻿namespace Assets.Scripts.Common
{
    public class Level
    {
        public Level(int number, int xpNeeded)
        {
            Number = number;
            XpNeeded = xpNeeded;
        }

        public int Number { get; private set; }

        public int XpNeeded { get; private set; }
    }
}
