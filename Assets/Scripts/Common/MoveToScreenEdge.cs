﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Common
{
    public  class MoveToScreenEdge : MonoBehaviour
    {
        void Start()
        {
            var screenEdge = Camera.main.ScreenToWorldPoint(new Vector2(0, 0));
            transform.position = new Vector2(screenEdge.x, transform.position.y);
        }
    }
}
