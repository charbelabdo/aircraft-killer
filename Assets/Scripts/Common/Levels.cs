﻿using System.Collections.Generic;

namespace Assets.Scripts.Common
{
    public static class Levels
    {
        private static Dictionary<int, Level> _levels = new Dictionary<int, Level>();

        static Levels()
        {
            AddLevel(0);
            AddLevel(1000);
            AddLevel(2000);
            AddLevel(4000);
            AddLevel(8000);
            AddLevel(12000);
            AddLevel(18000);
            AddLevel(24000);
            AddLevel(32000);
            AddLevel(40000);
            AddLevel(50000);
            AddLevel(62000);
            AddLevel(75000);
            AddLevel(90000);
            AddLevel(105000);
            AddLevel(120000);
            AddLevel(140000);
            AddLevel(165000);
            AddLevel(200000);
            AddLevel(240000);
            AddLevel(290000);
            AddLevel(350000);
            AddLevel(420000);
            AddLevel(500000);
            AddLevel(600000);
            AddLevel(700000);
            AddLevel(800000);
            AddLevel(900000);
            AddLevel(1000000);
            AddLevel(1200000);
            AddLevel(1500000);
            AddLevel(1800000);
            AddLevel(2000000);
        }

        private static void AddLevel(int xpNeeded)
        {
            _levels.Add(_levels.Count + 1, new Level(_levels.Count + 1, xpNeeded));
        }

        /// <summary>
        /// Gets a level object given the level number
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public static Level GetLevel(int level)
        {
            if (level < _levels.Count)
            {
                return _levels[level];
            }

            return null;
        }

        /// <summary>
        /// Checks if, given additional xp and the current level, the player should level up
        /// </summary>
        /// <param name="currentLevel">The current level of the player</param>
        /// <param name="xp">The current total xp of the player</param>
        /// <returns>True if the player should level up</returns>
        public static bool ShouldLevelUp(int currentLevel, int xp)
        {
            if (currentLevel == _levels.Count)
            {
                //No more levels
                return false;
            }

            var nextLevel = _levels[currentLevel + 1];
            return nextLevel.XpNeeded <= xp;
        }
    }
}
