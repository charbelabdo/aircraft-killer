﻿using UnityEngine;

namespace Assets.Scripts.Common
{
    public class VelocityRotate : MonoBehaviour
    {
        private Rigidbody2D _rigidbody2D;

        void Start()
        {
            _rigidbody2D = GetComponent<Rigidbody2D>();
        }

        void Update()
        {
            var speedVector = _rigidbody2D.velocity.normalized;

            var angle = Mathf.Atan2(speedVector.y, speedVector.x) * Mathf.Rad2Deg;
            var currentAngle = transform.rotation.eulerAngles.z;

            transform.RotateAround(transform.position, new Vector3(0, 0, 1),  (angle - currentAngle));

        }
    }
}
