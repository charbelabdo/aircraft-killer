﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Common
{
    public class Events : MonoBehaviour
    {
        private static Events _instance;
        public static Events Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<Events>();
                }

                return _instance;
            }
        }

        private readonly List<IEventResponder> _eventResponders = new List<IEventResponder>();

        public void Register(IEventResponder responder)
        {
            if (!_eventResponders.Contains(responder))
            {
                _eventResponders.Add(responder);
            }
        }

        public void Unregister(IEventResponder responder)
        {
            _eventResponders.Remove(responder);
        }

        public void TriggerEvent(EventType eventType)
        {
            foreach (var eventResponder in _eventResponders)
            {
                if (eventResponder != null)
                {
                    eventResponder.OnEvent(eventType);
                }
            }
        }

    }

    public interface IEventResponder
    {
        void OnEvent(EventType eventType);
    }

    public enum EventType
    {
        GoldAdded,
        XpAdded,
        LevelUp
    }
}
