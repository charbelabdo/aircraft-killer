﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.SaveAndLoad;

namespace Assets.Scripts
{
    public class PlayerManager
    {
        public Player Player { get; private set; }

        private static PlayerManager _instance;
        public static PlayerManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new PlayerManager();
                    var loader = new GameLoader();
                    if (loader.SaveGameExists())
                    {
                       _instance.Player = loader.Load();
                    }
                    else
                    {
                        _instance.Player = new NewGameCreator().CreateNewPlayer();
                    }
                }

                return _instance;
            }
        }

    }
}
