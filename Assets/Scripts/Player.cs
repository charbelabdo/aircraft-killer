﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    [Serializable]
    public class Player
    {
        public List<string> OwnedTurretIdentifiers;

        public List<string> CompletedBattleIdentifiers;

        public int Gold;

        public int CurrentLevel;

        public int CurrentXp;

    }
}
