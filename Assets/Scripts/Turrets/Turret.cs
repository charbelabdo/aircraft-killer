﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Projectiles;
using UnityEngine;
using Plane = Assets.Scripts.PlaneMechanics.Plane;

namespace Assets.Scripts.Turrets
{
    public class Turret : MonoBehaviour
    {
        private float _lastAngle = 0f;
        private Vector2 _directionVector = Vector2.right;
        private Camera _mainCamera;

        public Vector2 DirectionVector
        {
            get { return _directionVector; }
        }

        void Start()
        {
            _mainCamera = Camera.main;
        }

        /// <summary>
        /// Turns the turret towards the mouse
        /// </summary>
        /// <returns>The world position of the mouse</returns>
        public Vector2 TurnTowardsMouse()
        {
            var mousePositionWorld = _mainCamera.ScreenToWorldPoint(Input.mousePosition);
            TurnTowards(mousePositionWorld);
            return mousePositionWorld;
        }

        public void TurnTowards(Vector3 point)
        {
            _directionVector = (point - transform.position).normalized;
            var angle = Vector2.SignedAngle(Vector2.right, _directionVector);

            if (angle < 0)
            {
                if (angle > -90)
                {
                    angle = 0;
                    _directionVector = Vector2.right;
                }
                else
                {
                    angle = 180;
                    _directionVector = Vector2.left;
                }
            }

            var rotationNeeded = angle - _lastAngle;
            transform.RotateAround(transform.position, new Vector3(0, 0, 1), rotationNeeded);
            _lastAngle = angle;
        }
    }
}
