﻿using UnityEngine;

namespace Assets.Scripts.Turrets
{
    public class UpgradeButton : MonoBehaviour
    {
        private TurretBase _currentTurret;
        public TurretBase TargetTurret { get; private set; }

        public void Set(TurretBase currentTurret, TurretBase targetTurretPrefab)
        {
            _currentTurret = currentTurret;
            TargetTurret = targetTurretPrefab;
        }

        void OnMouseDown()
        {
            _currentTurret.UpgradeMe(TargetTurret);
        }
    }
}
