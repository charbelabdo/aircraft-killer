﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Turrets
{
    public class TurretBuildPopup : MonoBehaviour
    {
        public BuildButton BuildButtonPrefab;
        public Button CloseButton;
        public GridLayoutGroup Grid;
        private TurretSlot _turretSlot;
        private float _timeScaleOnOpen;

        public TurretSlot TurretSlot
        {
            get { return _turretSlot; }
        }

        void Start()
        {
            CloseButton.onClick.AddListener(Close);
        }

        public void Open(TurretSlot turretSlot)
        {
            _timeScaleOnOpen = Time.timeScale;
            Time.timeScale = 0f;
            _turretSlot = turretSlot;

            //Remove all children buttons;
            var childrenButtons = GetComponentsInChildren<BuildButton>();
            foreach (var childrenButton in childrenButtons)
            {
                Destroy(childrenButton.gameObject);
            }

            //Get available turrets
            var available = Game.Instance.TurretTracker.RootTurrets;

            //Create a button for each of them
            foreach (var turretBase in available)
            {
                var button = Instantiate(BuildButtonPrefab);
                button.SetTurret(turretBase, this);
                button.transform.SetParent(Grid.transform);
            }

            gameObject.SetActive(true);
        }

        public void Close()
        {
            Time.timeScale = _timeScaleOnOpen;
            gameObject.SetActive(false);
        }
    }
}