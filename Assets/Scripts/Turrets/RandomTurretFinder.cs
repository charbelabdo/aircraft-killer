﻿namespace Assets.Scripts.Turrets
{
    public class RandomTurretFinder
    {
        public TurretBase GetTurret()
        {
            return Game.Instance.TurretTracker.GetRandomTurret();
        }
    }
}
