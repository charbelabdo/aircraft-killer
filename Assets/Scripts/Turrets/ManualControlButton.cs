﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Turrets
{
    public class ManualControlButton : MonoBehaviour
    {
        public bool ManualControl = false;
        private SpriteRenderer _spriteRenderer;

        void Awake()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }

        void OnMouseDown()
        {
            ManualControl = !ManualControl;
            if (ManualControl)
            {
                _spriteRenderer.color = Color.green;
            }
            else
            {
                _spriteRenderer.color = Color.white;
            }
        }
    }
}
