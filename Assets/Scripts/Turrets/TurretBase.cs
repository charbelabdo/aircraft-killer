﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Common;
using UnityEngine;
using EventType = Assets.Scripts.Common.EventType;

namespace Assets.Scripts.Turrets
{
    public class TurretBase : MonoBehaviour, IEventResponder
    {
        public TurretData TurretData;
        private List<TurretBase> _possibleUpgrades = new List<TurretBase>();
        private UpgradeButton _upgradeButton;

        void Awake()
        {
            this.gameObject.tag = Tags.TurretTag;
            _upgradeButton = GetComponentInChildren<UpgradeButton>();
            
        }

        void Start()
        {
            Game.Instance.TurretTracker.RegisterTurret(this);
            _possibleUpgrades = Game.Instance.TurretTracker.GetUpgrades(this);
            Events.Instance.Register(this);
            if (_upgradeButton != null && _possibleUpgrades.Any())
            {
                _upgradeButton.Set(this, _possibleUpgrades.First());
            }
            else if (_upgradeButton != null && !_possibleUpgrades.Any())
            {
                Destroy(_upgradeButton.gameObject);
                _upgradeButton = null;
            }

            if (_upgradeButton != null)
            {
                _upgradeButton.gameObject.SetActive(Game.Instance.CurrentGold >= _upgradeButton.TargetTurret.TurretData.GoldCost);
            }
        }


        void OnDestroy()
        {
            if (Game.Instance == null || Game.Instance.TurretTracker == null)
            {
                return;
            }

            Game.Instance.TurretTracker.UnregisterTurret(this);
        }

        public void UpgradeMe(TurretBase upgrade)
        {
            var newTurret = Instantiate(upgrade);
            newTurret.gameObject.transform.position = transform.position;
            newTurret.gameObject.transform.SetParent(gameObject.transform.parent, true);

            var currentManualControl = GetComponentInChildren<ManualControlButton>();
            if (currentManualControl != null)
            {
                var manualControl = newTurret.GetComponentInChildren<ManualControlButton>();
                if (manualControl != null)
                {
                    manualControl.ManualControl = currentManualControl.ManualControl;
                }
            }

            Destroy(gameObject);
        }

        public void OnEvent(EventType eventType)
        {
            if (eventType == EventType.GoldAdded)
            {
                if (_upgradeButton != null && _upgradeButton.TargetTurret.TurretData.GoldCost <= Game.Instance.CurrentGold)
                {
                    _upgradeButton.gameObject.SetActive(true);
                }
                else if (_upgradeButton != null)
                {
                    _upgradeButton.gameObject.SetActive(false);
                }
            }
        }
    }
}
