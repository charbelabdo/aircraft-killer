﻿using Assets.Scripts.PlaneMechanics;

namespace Assets.Scripts.Turrets
{
    public class RandomTargetFinder
    {
        public Plane GetRandomTarget()
        {
            return Game.Instance.PlaneTracker.GetRandomPlane();
        }
    }
}
