﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Turrets
{
    [CreateAssetMenu]
    public  class TurretData : ScriptableObject
    {
        public string Identifier;

        public string Name;

        public Sprite Sprite;

        public int GoldCost;

        public TurretData UpgradeFrom;
    }
}
