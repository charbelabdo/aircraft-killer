﻿using Assets.Scripts.Common;
using UnityEngine;

namespace Assets.Scripts.Turrets
{
    public class TurretSlot : MonoBehaviour
    {
        public void BuildTurret(TurretBase turretPrefab)
        {
            var turret = Instantiate(turretPrefab);
            turret.transform.position = gameObject.transform.position;
            turret.gameObject.transform.SetParent(Game.Instance.TurretTracker.transform, true);
            Game.Instance.AddGold(-turretPrefab.TurretData.GoldCost);
            Destroy(gameObject);
        }

        void OnMouseDown()
        {
            PopupManager.Instance.OpenTurretBuildPopup(this);
        }
    }
}
