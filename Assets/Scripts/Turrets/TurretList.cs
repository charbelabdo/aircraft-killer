﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Turrets
{
    public class TurretList : MonoBehaviour
    {
        public List<TurretBase> Turrets;

        private static TurretList _instance;
        public static TurretList Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<TurretList>();
                }

                return _instance;
            }
        }
    }
}
