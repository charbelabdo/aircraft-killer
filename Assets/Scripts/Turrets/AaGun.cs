﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Common;
using Assets.Scripts.Projectiles;
using UnityEngine;

namespace Assets.Scripts.Turrets
{
    [RequireComponent(typeof(Turret))]
    public class AaGun : MonoBehaviour
    {
        private const float BulletSpeed = 100;

        private List<Muzzle> _muzzles;
        private Turret _turret;
        private AudioSource _audioSource;

        private float _timeSinceLastFire = 1000f;
        [Tooltip("The rate of fire - how many seconds between each bullet")]
        public float RateOfFire = 0.5f;
        public Bullet BulletPrefab;

        private RandomTargetFinder _randomTargetFinder;
        public ManualControlButton ManualControlButton;
        private bool ManualControl
        {
            get { return ManualControlButton.ManualControl; }
        }

        void Start()
        {
            _muzzles = GetComponentsInChildren<Muzzle>().ToList();
            _turret = GetComponent<Turret>();
            _randomTargetFinder = new RandomTargetFinder();
            _audioSource = GetComponent<AudioSource>();
        }

        void Update()
        {
            _timeSinceLastFire += Time.deltaTime;

            if (!ManualControl)
            {
                if (_timeSinceLastFire >= RateOfFire)
                {
                    var target = _randomTargetFinder.GetRandomTarget();
                    if (target != null)
                    {
                        _turret.TurnTowards(target.transform.position);
                        Fire();
                    }
                }
            }
            else
            {
                _turret.TurnTowardsMouse();
                if (_timeSinceLastFire > RateOfFire)
                {
                    if (Input.GetMouseButton(0))
                    {
                        Fire();
                    }
                }
            }
        }

        private void Fire()
        {
            _audioSource.time = 0;
            _audioSource.Play();

            foreach (var muzzle in _muzzles)
            {
                var newBullet = Instantiate(BulletPrefab);
                newBullet.gameObject.layer = LayerMask.NameToLayer(Layers.AllyBullets);
                newBullet.transform.position = muzzle.transform.position;
                var bulletRigidBody = newBullet.GetComponent<Rigidbody2D>();
                bulletRigidBody.velocity = _turret.DirectionVector * BulletSpeed;
                _timeSinceLastFire = 0f;
            }
        }
    }
}
