﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Turrets
{
    public class BuildButton : MonoBehaviour
    {
        private Button _button;
        public Text NameText;
        public Text CostText;
        private TurretBase _turret;
        private TurretBuildPopup _turretBuildPopup;

        void Awake()
        {
            _button = GetComponent<Button>();
        }

        public void SetTurret(TurretBase turret, TurretBuildPopup parentPopup)
        {
            _turret = turret;
            NameText.text = turret.TurretData.Name;
            CostText.text = turret.TurretData.GoldCost.ToString("0");

            if (turret.TurretData.GoldCost > Game.Instance.CurrentGold)
            {
                _button.interactable = false;
            }

            _turretBuildPopup = parentPopup;
            _button.onClick.RemoveAllListeners();
            _button.onClick.AddListener(() =>
            {
                parentPopup.TurretSlot.BuildTurret(turret);
                parentPopup.Close();
            });
        }
    }
}
