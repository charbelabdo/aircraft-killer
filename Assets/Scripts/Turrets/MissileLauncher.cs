﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Common;
using Assets.Scripts.Projectiles;
using UnityEngine;

namespace Assets.Scripts.Turrets
{
    [RequireComponent(typeof(Turret))]
    public class MissileLauncher : MonoBehaviour
    {
        public Missile MissilePrefab;

        [Tooltip("The rate of fire - how many seconds between each bullet")]
        public float RateOfFire = 2f;
        private float _timeSinceLastFire = 1000f;

        private Turret _turret;
        private List<Muzzle> _muzzles;
        private RandomTargetFinder _randomTargetFinder;

        public ManualControlButton ManualControlButton;
        private bool ManualControl
        {
            get { return ManualControlButton.ManualControl; }
        }

        void Start()
        {
            _turret = GetComponent<Turret>();
            _muzzles = GetComponentsInChildren<Muzzle>().ToList();
            _randomTargetFinder = new RandomTargetFinder();
        }

        void Update()
        {
            _timeSinceLastFire += Time.deltaTime;
            if (!ManualControl)
            {
                if (_timeSinceLastFire >= RateOfFire)
                {
                    var target = _randomTargetFinder.GetRandomTarget();
                    if (target != null)
                    {
                        _turret.TurnTowards(target.transform.position);
                        LaunchMissiles();
                    }
                }
            }
            else
            {
                var mousePosition = _turret.TurnTowardsMouse();
                if (Input.GetMouseButtonUp(0))
                {
                    if (_timeSinceLastFire >= RateOfFire && mousePosition.y >= transform.position.y)
                    {
                        LaunchMissiles();
                    }
                }
            }
        }

        public void LaunchMissiles()
        {
            _timeSinceLastFire = 0f;
            var direction = _turret.DirectionVector;
            foreach (var muzzle in _muzzles)
            {
                var newMissile = Instantiate(MissilePrefab);
                newMissile.gameObject.layer = LayerMask.NameToLayer(Layers.AllyProjectiles);
                newMissile.transform.position = muzzle.transform.position;
                newMissile.Launch(direction);
                newMissile.gameObject.tag = Tags.TurretTag;
            }
        }
    }
}
