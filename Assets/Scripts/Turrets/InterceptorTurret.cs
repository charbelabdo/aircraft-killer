﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Common;
using Assets.Scripts.Projectiles;
using UnityEngine;

namespace Assets.Scripts.Turrets
{
    [RequireComponent(typeof(Turret))]
    public class InterceptorTurret : MonoBehaviour
    {
        public float FireRate = 1f;
        public float InterceptionRange = 50f;
        public Missile MissilePrefab;

        private float _timeSinceLastFire = 1000f;
        private LayerMask _layerMask;
        private Turret _turret;
        private List<Muzzle> _muzzles;

        void Awake()
        {
            _turret = GetComponent<Turret>();
            _muzzles = GetComponentsInChildren<Muzzle>().ToList();
        }

        void Start()
        {
            _layerMask = LayerMask.GetMask(Layers.EnemyProjectiles);
        }

        void Update()
        {
            _timeSinceLastFire += Time.deltaTime;
            if (_timeSinceLastFire >= FireRate)
            {
                var targets = Physics2D.OverlapCircleAll(transform.position, InterceptionRange, _layerMask);
                foreach (var target in targets)
                {
                    if (target.tag != Tags.EnemyTag)
                    {
                        continue;
                    }

                    var missile = target.GetComponent<Missile>();
                    if (missile != null)
                    {
                        _timeSinceLastFire = 0f;
                        var direction = _turret.DirectionVector;
                        _turret.TurnTowards(missile.transform.position);
                        foreach (var muzzle in _muzzles)
                        {
                            var newMissile = Instantiate(MissilePrefab);
                            newMissile.gameObject.layer = LayerMask.NameToLayer(Layers.AllyProjectiles);
                            newMissile.transform.position = muzzle.transform.position;
                            newMissile.gameObject.tag = Tags.TurretTag;
                            newMissile.SetTarget(missile.gameObject);
                            newMissile.Launch(direction);
                        }
                    }
                }
            }
        }
    }
}
