﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Turrets
{
    public class TurretTracker : MonoBehaviour
    {
        private readonly List<TurretBase> _availableTurretPrefabs = new List<TurretBase>();
        private readonly List<TurretBase> _turrets = new List<TurretBase>();
        private List<TurretBase> _rootTurrets = new List<TurretBase>();

        public bool LoseWhenAllTurretsDestroyed = true;

        void Awake()
        {
            var playerAvailableTurrets = PlayerManager.Instance.Player.OwnedTurretIdentifiers;
            foreach (var playerAvailableTurret in playerAvailableTurrets)
            {
                var prefab = TurretList.Instance.Turrets.FirstOrDefault(x => x.TurretData.Identifier == playerAvailableTurret);
                if (prefab != null)
                {
                    _availableTurretPrefabs.Add(prefab);
                }
            }

            _rootTurrets = _availableTurretPrefabs.Where(x => x.TurretData.UpgradeFrom == null).ToList();
        }

        public List<TurretBase> RootTurrets
        {
            get { return _rootTurrets; }
        }

        public List<TurretBase> GetUpgrades(TurretBase turret)
        {
            return _availableTurretPrefabs.Where(x => x.TurretData.UpgradeFrom == turret.TurretData).ToList();
        }

        public TurretBase GetRandomTurret()
        {
            if (!_turrets.Any())
            {
                return null;
            }

            var rnd = Random.Range(0, _turrets.Count);
            return _turrets[rnd];
        }

        public void RegisterTurret(TurretBase turretBase)
        {
            _turrets.Add(turretBase);
        }

        public void UnregisterTurret(TurretBase turretBase)
        {
            _turrets.Remove(turretBase);

            if (LoseWhenAllTurretsDestroyed && !_turrets.Any())
            {
                Game.Instance.Lose();
            }
        }
    }
}
